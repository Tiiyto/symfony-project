# Site web pour l'association "La Flèche Millamoise"

## Installer le site sur sa machine

Nécessite le manager de dépendance [Composer](https://getcomposer.org/)

### Cloner le dépot

```bash
git clone https://github.com/Tiiyto/Fleche.git
```

### Se mettre dans le dossier

```bash
cd Fleche
```

### Installer les dépendances

```bash
composer install
```

### Création de la base de données (mysql)

```bash
php bin/console doctrine:database:create
```

### On exécute les migrations

```bash
php bin/console doctrine:migrations:migrate
```

### Éxecution des fixtures

```bash
php bin/console doctrine:fixtures:load --no-interaction
```

### On lance le serveur

```bash
php bin/console server:run
```

Ou si cela ne fonctionne pas

```bash
php -S localhost:8000 -t public
```

### Ensuite accès au site via l'url:

[http://localhost:8000/](http://localhost:8000/)

## Essai du site

Compte Administrateur d'essai :

- Admin@Admin.fr
- root

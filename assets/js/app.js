/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "../css/app.css";

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

$(document).ready(function () {
  $("a.gallery").featherlightGallery({
    previousIcon: '<i class="fas fa-arrow-left"></i>',
    nextIcon: '<i class="fas fa-arrow-right"></i>',
    galleryFadeIn: 300,

    openSpeed: 300,
  });
});

$.ajax({
  url: "https://www.prevision-meteo.ch/services/json/Millam",
  method: "get",
  dataType: "json",
  success: function (data, statut) {
    document.getElementById("chargement-js").style.display = "none";
    document.getElementById("js-meteo").style.display = "block";

    switch (data.fcst_day_1.day_short) {
      case "Lun.":
        var j1 = "Lundi";
        var j2 = "Mardi";
        break;
      case "Mar.":
        var j1 = "Mardi";
        var j2 = "Mercredi";
        break;
      case "Mer.":
        var j1 = "Mercredi";
        var j2 = "Jeudi";
        break;
      case "Jeu.":
        var j1 = "Jeudi";
        var j2 = "Vendredi";
        break;
      case "Ven.":
        var j1 = "Vendredi";
        var j2 = "Samedi";
        break;
      case "Sam.":
        var j1 = "Samedi";
        var j2 = "Dimanche";
        break;
      case "Dim.":
        var j1 = "Dimanche";
        var j2 = "Lundi";
        break;
    }
    $("#icon-js").html('<img src="' + data.fcst_day_0.icon_big + '">');
    $("#infos-js").html(
      "<span class='bold'>Aujourd'hui</span>" +
        '<br><span class="min bold">' +
        data.fcst_day_0.tmin +
        "°C </span>" +
        '<span class="max bold">' +
        data.fcst_day_0.tmax +
        "°C </span>" +
        "<br>" +
        '<img class="wind" src="https://img.icons8.com/ios/28/000000/wind.png" id >' +
        '<span class="bold">' +
        data.current_condition.wnd_spd +
        " km/h" +
        "</span>"
    );
    $("#js-weather1").html(
      '<span class="bold mr-1">' +
        j1 +
        "</span>" +
        '<img src="' +
        data.fcst_day_1.icon +
        '">' +
        '<br><span class="min bold"> ' +
        data.fcst_day_1.tmin +
        "°C </span>" +
        '<span class="max bold"> ' +
        data.fcst_day_1.tmax +
        "°C </span>"
    );
    $("#js-weather2").html(
      '<span class="bold mr-1">' +
        j2 +
        "</span>" +
        '<img src="' +
        data.fcst_day_2.icon +
        '">' +
        '<br><span class="min bold"> ' +
        data.fcst_day_2.tmin +
        "°C </span>" +
        '<span class="max bold"> ' +
        data.fcst_day_2.tmax +
        "°C </span>"
    );
  },
});

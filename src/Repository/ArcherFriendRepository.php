<?php

namespace App\Repository;

use App\Entity\ArcherFriend;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ArcherFriend|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArcherFriend|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArcherFriend[]    findAll()
 * @method ArcherFriend[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArcherFriendRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArcherFriend::class);
    }

    // /**
    //  * @return ArcherFriend[] Returns an array of ArcherFriend objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArcherFriend
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

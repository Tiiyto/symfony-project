<?php

namespace App\Repository;

use App\Entity\Albums;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Albums|null find($id, $lockMode = null, $lockVersion = null)
 * @method Albums|null findOneBy(array $criteria, array $orderBy = null)
 * @method Albums[]    findAll()
 * @method Albums[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Albums::class);
    }

    /**
     * @return Query
     */
    public function findAllP(): Query
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.public = 1')
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }
    public function findP($id): ?Albums
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.public = 1')
            ->andWhere('a.id = :id ')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findAllPr(): Query
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.public = 0')
            ->orderBy('a.id', 'DESC')
            ->getQuery();
    }
    public function findPr($id): ?Albums
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.public = 0')
            ->andWhere('a.id = :id ')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findLastOne(): ?Albums
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.updatedAt', 'DESC')
            ->andWhere('a.public = 1')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Albums[] Returns an array of Albums objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Albums
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

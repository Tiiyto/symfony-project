<?php

namespace App\Repository;

use App\Entity\Forum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


/**
 * @method Forum|null find($id, $lockMode = null, $lockVersion = null)
 * @method Forum|null findOneBy(array $criteria, array $orderBy = null)
 * @method Forum[]    findAll()
 * @method Forum[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Forum::class);
    }

    /**
     * @return Forum[] Returns an array of Forum objects
     */

    public function findAllVisible()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.Visible = 1')
            ->orderBy('f.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
    public function findAllSearchedVisible(?string $searched)
    {
        return $this->createQueryBuilder('f')
            ->andWhere(' f.Visible = 1 AND
                f.title LIKE :searchTerm
                OR f.Description LIKE :searchTerm
                OR fa.Description LIKE :searchTerm')
            ->leftJoin('f.forumAnswers', 'fa')
            ->setParameter('searchTerm', '%' . $searched . '%')
            ->orderBy('f.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Forum
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

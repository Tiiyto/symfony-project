<?php

namespace App\Repository;

use App\Entity\PublicBooking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PublicBooking|null find($id, $lockMode = null, $lockVersion = null)
 * @method PublicBooking|null findOneBy(array $criteria, array $orderBy = null)
 * @method PublicBooking[]    findAll()
 * @method PublicBooking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PublicBookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PublicBooking::class);
    }
    public function getNextEvent()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.beginAt', 'DESC')
            ->andWhere('p.beginAt > CURRENT_DATE()')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    // /**
    //  * @return PublicBooking[] Returns an array of PublicBooking objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PublicBooking
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

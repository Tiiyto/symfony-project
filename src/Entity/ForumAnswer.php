<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ForumAnswerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ForumAnswer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Forum", inversedBy="forumAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Forum;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="forumAnswers")
     */
    private $Author;

    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->createdAt = new \DateTime();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getForum(): ?Forum
    {
        return $this->Forum;
    }

    public function setForum(?Forum $Forum): self
    {
        $this->Forum = $Forum;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->Author;
    }

    public function setAuthor(?User $Author): self
    {
        $this->Author = $Author;

        return $this;
    }
    public function __toString()
    {
        return $this->Description;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ForumRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Forum
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Author;

    /**
     * @ORM\Column(type="text")
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ForumAnswer", mappedBy="Forum", orphanRemoval=true)
     */
    private $forumAnswers;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Visible;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;


    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->Visible = 0;
        $this->forumAnswers = new ArrayCollection();
    }
    /**
     * @ORM\PreUpdate
     */
    public function updateUpdatedAt(): self
    {
        date_default_timezone_set('Europe/Paris');
        if (end($this->forumAnswers) == null)
            $this->updatedAt = $this->createdAt;
        else
            $this->updatedAt = end($this->forumAnswers)->getCreatedAt;
        return $this;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->Author;
    }

    public function setAuthor(string $Author): self
    {
        $this->Author = $Author;

        return $this;
    }
    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|ForumAnswer[]
     */
    public function getForumAnswers(): Collection
    {
        return $this->forumAnswers;
    }
    public function addForumAnswer(ForumAnswer $forumAnswer): self
    {
        if (!$this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers[] = $forumAnswer;
            $forumAnswer->setForum($this);
        }

        return $this;
    }

    public function removeForumAnswer(ForumAnswer $forumAnswer): self
    {
        if ($this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers->removeElement($forumAnswer);
            // set the owning side to null (unless already changed)
            if ($forumAnswer->getForum() === $this) {
                $forumAnswer->setForum(null);
            }
        }

        return $this;
    }
    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->title);
    }

    public function getVisible(): ?bool
    {
        return $this->Visible;
    }

    public function setVisible(bool $Visible): self
    {
        $this->Visible = $Visible;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
    public function __toString()
    {
        return $this->title;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FriendRepository")
 * @Vich\Uploadable
 */
class Friend
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SiteUrl;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string|null
     */
    private $filename;

    /**
     * @Vich\UploadableField(mapping="album_image", fileNameProperty="filename", size="imageSize")
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $imageSize;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }
    public function setName(?string $Name): ?self
    {
        $this->Name = $Name;

        return $this;
    }
    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getSiteUrl(): ?string
    {
        return $this->SiteUrl;
    }

    public function setSiteUrl(?string $SiteUrl): self
    {
        $this->SiteUrl = $SiteUrl;

        return $this;
    }
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }
    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }
    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
    public function __toString()
    {
        return $this->filename;
    }
}

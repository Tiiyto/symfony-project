<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Password;

    /** 
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\Column(type="integer")
     */
    private $Groupe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Booking", mappedBy="Participant")
     */
    private $Event;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ForumAnswer", mappedBy="Author",cascade={"remove"})
     */
    private $forumAnswers;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    public function __construct()
    {
        $this->Groupe = 2;
        $this->Event = new ArrayCollection();
        $this->forumAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->Password;
    }

    public function setPassword(string $Password): self
    {
        $this->Password = $Password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }
    public function getUsername(): ?string
    {
        return $this->Email;
    }


    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getGroupe(): ?int
    {
        return $this->Groupe;
    }

    public function setGroupe(int $Groupe): self
    {
        $this->Groupe = $Groupe;

        return $this;
    }
    public function getRoles()
    {
        if ($this->Groupe == 1)
            return ['ROLE_ADMIN'];
        else if ($this->Groupe == 2)
            return ['ROLE_USER'];
    }
    public function getSalt()
    {
        return null;
    }
    public function eraseCredentials()
    {
    }
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->Email,
            $this->Password
        ]);
    }
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->Email,
            $this->Password
        ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return Collection|Booking[]
     */
    public function getEvent(): Collection
    {
        return $this->Event;
    }

    public function addEvent(Booking $event): self
    {
        if (!$this->Event->contains($event)) {
            $this->Event[] = $event;
            $event->addParticipant($this);
        }

        return $this;
    }

    public function removeEvent(Booking $event): self
    {
        if ($this->Event->contains($event)) {
            $this->Event->removeElement($event);
            $event->removeParticipant($this);
        }

        return $this;
    }
    public function __toString(): ?string
    {
        $firstnameAndName = $this->firstname . ' ' . $this->Name;
        return $firstnameAndName;
    }

    /**
     * @return Collection|ForumAnswer[]
     */
    public function getForumAnswers(): Collection
    {
        return $this->forumAnswers;
    }

    public function addForumAnswer(ForumAnswer $forumAnswer): self
    {
        if (!$this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers[] = $forumAnswer;
            $forumAnswer->setAuthor($this);
        }

        return $this;
    }

    public function removeForumAnswer(ForumAnswer $forumAnswer): self
    {
        if ($this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers->removeElement($forumAnswer);
            // set the owning side to null (unless already changed)
            if ($forumAnswer->getAuthor() === $this) {
                $forumAnswer->setAuthor(null);
            }
        }

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Albums", inversedBy="IdImage")
     * @ORM\JoinColumn(nullable=false)
     */
    private $IdAlbum;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $filename;

    /**
     * @Vich\UploadableField(mapping="album_image", fileNameProperty="filename", size="imageSize")
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValue()
    {
        date_default_timezone_set('Europe/Paris');
        $alb = $this->getIdAlbum();
        $new = new \DateTime();
        $alb->setUpdatedAt($new);
    }
    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->createdAt = new \DateTime();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAlbum(): ?Albums
    {
        return $this->IdAlbum;
    }

    public function setIdAlbum(?Albums $IdAlbum): self
    {
        $this->IdAlbum = $IdAlbum;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }
    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }
    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
    public function __toString()
    {
        return $this->filename;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlbumsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Albums
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="IdAlbum", orphanRemoval=true)
     */
    private $IdImage;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $public;

    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        $this->createdAt =  new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->IdImage = new ArrayCollection();
    }
    /**
     * @ORM\PreUpdate
     */
    public function updateUpdatedAt()
    {
        date_default_timezone_set('Europe/Paris');
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->title);
    }

    /**
     * @return Collection|Image[]
     */
    public function getIdImage(): Collection
    {
        return $this->IdImage;
    }
    public function getLastImage()
    {
        $maxIm = $this->IdImage[0];
        foreach ($this->IdImage as $Im) {
            if ($maxIm->getCreatedAt() < $Im->getCreatedAt()) {
                $maxIm = $Im;
            }
        }
        return $maxIm;
    }
    public function addIdImage(Image $idImage): self
    {
        if (!$this->IdImage->contains($idImage)) {
            $this->IdImage[] = $idImage;
            $idImage->setIdAlbum($this);
        }
        return $this;
    }
    public function removeIdImage(Image $idImage): self
    {
        if ($this->IdImage->contains($idImage)) {
            $this->IdImage->removeElement($idImage);
            // set the owning side to null (unless already changed)
            if ($idImage->getIdAlbum() === $this) {
                $idImage->setIdAlbum(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->title;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }
}

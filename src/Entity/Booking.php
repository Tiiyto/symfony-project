<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */

    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="Event")
     */
    private $Participant;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    public function __construct()
    {
        $begin = new \DateTime();
        $begin->setTime(8, 0);
        $this->beginAt = $begin;
        $end = new \DateTime();
        $end->setTime(8, 0);
        $end->modify('+1 day');
        $this->endAt = $end;
        $this->Participant = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
    /**
     * @return Collection|User[]
     */
    public function getParticipant(): Collection
    {
        return $this->Participant;
    }

    public function getParticipation(User $participant): bool
    {
        $participation = false;
        if ($this->Participant->contains($participant)) {
            $participation = true;
        }
        return $participation;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->Participant->contains($participant)) {
            $this->Participant[] = $participant;
        }
        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        if ($this->Participant->contains($participant)) {
            $this->Participant->removeElement($participant);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }
}

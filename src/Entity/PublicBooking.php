<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PublicBookingRepository")
 * @ApiResource(
 *      normalizationContext={"groups"={"read:event"}}, 
 *      collectionOperations={"get"},
 *      itemOperations={"get"}
 * )
 */
class PublicBooking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read:event"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:event"})
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"read:event"})
     */
    private $endAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:event"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read:event"})
     */
    private $Description;

    public function __construct()
    {
        $begin = new \DateTime();
        $begin->setTime(8, 0);
        $this->beginAt = $begin;
        $end = new \DateTime();
        $end->setTime(8, 0);
        $end->modify('+1 day');
        $this->endAt = $end;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }
}

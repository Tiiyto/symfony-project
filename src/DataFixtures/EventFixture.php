<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\PublicBooking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class EventFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 5; $i++) {
            $begin = $faker->dateTimeInInterval('now', '+ 20 days', null);
            $Booking = new Booking();
            $Booking->setTitle($faker->words(3, true))
                ->setBeginAt($begin)
                ->setEndAt($faker->dateTimeInInterval($begin, '+ 15 days', null))
                ->setDescription($faker->paragraph(3, true));
            $manager->persist($Booking);
        }
        for ($i = 0; $i < 5; $i++) {
            $begin = $faker->dateTimeInInterval('now', '+ 20 days', null);
            $PublicBooking = new PublicBooking();
            $PublicBooking->setTitle($faker->words(3, true))
                ->setBeginAt($begin)
                ->setEndAt($faker->dateTimeInInterval($begin, '+ 15 days', null))
                ->setDescription($faker->paragraph(3, true));
            $manager->persist($PublicBooking);
        }
        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Forum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ForumFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 25; $i++) {
            $forum = new Forum();
            $forum->setTitle($faker->words(3, true))
                ->setDescription($faker->paragraph(3, true))
                ->setVisible($faker->numberBetween(0, 1))
                ->setAuthor('Anonyme');
            $manager->persist($forum);
        }
        $manager->flush();
    }
}

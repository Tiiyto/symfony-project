<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('admin@admin.fr')
            ->setName('Admin Admin')
            ->setGroupe(1)
            ->setPassword($this->encoder->encodePassword($admin, 'admin'));
        $manager->persist($admin);
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 15; $i++) {
            $user = new User;
            $user->setEmail($faker->email)
                ->setName($faker->firstName)
                ->setGroupe(2)
                ->setPassword($this->encoder->encodePassword($user, 'password'));
            $manager->persist($user);
        }
        $manager->flush();
    }
}

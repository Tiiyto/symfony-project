<?php

namespace App\DataFixtures;

use App\Entity\Albums;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AlbumFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 60; $i++) {
            $album = new Albums;
            $album->setTitle($faker->words(3, true))
                ->setPublic($faker->numberBetween(0, 1));
            $manager->persist($album);
        }
        $album = new Albums;
        $album->setTitle('Contient des photos')
            ->setPublic(1);
        $manager->persist($album);
        $manager->flush();
    }
}

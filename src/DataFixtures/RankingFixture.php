<?php

namespace App\DataFixtures;

use App\Entity\Ranking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class RankingFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 45; $i++) {
            $classement = new Ranking;
            $classement->setFirstname($faker->firstName)
                ->setLastname($faker->lastName)
                ->setScore($faker->numberBetween(0, 100));
            $manager->persist($classement);
        }
        $manager->flush();
    }
}

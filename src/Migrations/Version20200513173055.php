<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200513173055 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE forum DROP FOREIGN KEY FK_852BBECDBDFC9701');
        $this->addSql('DROP INDEX IDX_852BBECDBDFC9701 ON forum');
        $this->addSql('ALTER TABLE forum DROP last_answer_id, DROP nb_messages');
        $this->addSql('ALTER TABLE friend CHANGE filename filename VARCHAR(255) NOT NULL, CHANGE image_size image_size INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD firstname VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE forum ADD last_answer_id INT DEFAULT NULL, ADD nb_messages INT NOT NULL');
        $this->addSql('ALTER TABLE forum ADD CONSTRAINT FK_852BBECDBDFC9701 FOREIGN KEY (last_answer_id) REFERENCES forum_answer (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_852BBECDBDFC9701 ON forum (last_answer_id)');
        $this->addSql('ALTER TABLE friend CHANGE filename filename VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE image_size image_size INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP firstname');
    }
}

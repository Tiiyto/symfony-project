<?php

namespace App\Controller;

use App\Repository\AlbumsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends AbstractController
{

    private $repository;

    /**
     *@return Response
     */
    public function __construct(AlbumsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/albums", name="list.albums")
     *@return Response
     */

    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $Albums = $paginator->paginate(
            $this->repository->findAllP(),
            $request->query->getInt('page', 1), /*page number*/
            9
        );
        return $this->render('album/index.html.twig', [
            'current_menu' => 'albums',
            'albums' => $Albums,
            'public' => 1
        ]);
    }

    /**
     * @Route("/albums/{slug}-{id}", name="album.show", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function show($slug, $id): Response
    {
        $album = $this->repository->findP($id);
        return $this->render('album/show.html.twig', [
            'album' => $album,
            'current_menu' => 'albums'
        ]);
    }
    /**
     * @Route("/profile/albums", name="user.list.albums")
     *@return Response
     */

    public function indexPrivate(PaginatorInterface $paginator, Request $request): Response
    {
        $Albums = $paginator->paginate(
            $this->repository->findAllPr(),
            $request->query->getInt('page', 1), /*page number*/
            9
        );
        return $this->render('album/index.html.twig', [
            'current_menu' => 'vieAsso',
            'albums' => $Albums,
            'public' => 0
        ]);
    }

    /**
     * @Route("/profile/albums/{slug}-{id}", name="user.album.show", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function showPrivate($slug, $id): Response
    {
        $album = $this->repository->findPr($id);
        return $this->render('album/show.html.twig', [
            'album' => $album,
            'current_menu' => 'vieAsso'
        ]);
    }
}

<?php

namespace App\Controller;

use App\Controller\Calendar\BookingController;
use App\Repository\AlbumsRepository;
use App\Repository\PublicBookingRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(UserRepository $userRepository, AlbumsRepository $albumsRepository, PublicBookingRepository $publicBookingRepository): Response
    {
        $Users = $userRepository->findAdmin();
        $lastAlbum = $albumsRepository->findLastOne();
        $lastPicture = null;
        $date = null;
        if ($lastAlbum != null) {
            $lastPicture = $lastAlbum->getLastImage();
            if ($lastPicture) $date = $lastPicture->getCreatedAt()->format('d/m à H:i');
        }
        $NextEvent = $publicBookingRepository->getNextEvent();
        return $this->render('pages/home.html.twig', [
            'admin' => $Users,
            'lastAlbum' => $lastAlbum,
            'lastPicture' => $lastPicture,
            'date' => $date,
            'NextEvent' => $NextEvent
        ]);
    }
}

<?php

namespace App\Controller\Calendar;

use App\Entity\Booking;
use App\Entity\PublicBooking;
use App\Repository\BookingRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class BookingController extends AbstractController
{
    /**
     * @Route("/profile/booking", name="booking_index", methods={"GET"})
     */
    public function index(BookingRepository $bookingRepository): Response
    {
        return $this->render('booking/index.html.twig', [
            'bookings' => $bookingRepository->findAll(),
            'current_menu' => 'vieAsso'
        ]);
    }
    /**
     * @param Request $request
     * @Route("/profile/calendar/download", name="user.calendar.dl", methods={"GET"})
     */
    public function downloadAction(BookingRepository $bookingRepository)
    {
        date_default_timezone_set('Europe/Berlin');
        $vCalendar = new \Eluceo\iCal\Component\Calendar('Flêche_Millamoise');
        $events = $bookingRepository->findAll();
        foreach ($events as  $event) {
            if ($event->getBeginAt() > new DateTime()) {
                $vEvent = new \Eluceo\iCal\Component\Event();
                $vEvent->setDtStart($event->getBeginAt())->setDtEnd($event->getEndAt())->setSummary($event->getTitle());
                $vEvent->setUseTimezone(true);
                $vCalendar->addComponent($vEvent);
            }
        }
        $response = new Response();
        $response->setContent($vCalendar->render())->setCharset('ISO-8859-1')->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'multipart/alternative');
        $response->headers->set('Content-Disposition', 'attachment; filename="cal.ics"');
        return $response;
    }
    /**
     * @param Request $request
     * @Route("/profile/calendar/downloadP", name="user.calendar.dlP", methods={"GET"})
     */
    public function downloadPAction()
    {
        $user = $this->getUser();
        date_default_timezone_set('Europe/Berlin');
        $vCalendar = new \Eluceo\iCal\Component\Calendar('Flêche_Millamoise');
        $events = $user->getEvent();
        foreach ($events as  $event) {
            if ($event->getBeginAt() > new DateTime()) {
                $vEvent = new \Eluceo\iCal\Component\Event();
                $vEvent->setDtStart($event->getBeginAt())->setDtEnd($event->getEndAt())->setSummary($event->getTitle());
                $vEvent->setUseTimezone(true);
                $vCalendar->addComponent($vEvent);
            }
        }
        $response = new Response();
        $response->setContent($vCalendar->render())->setCharset('ISO-8859-1')->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'multipart/alternative');
        $response->headers->set('Content-Disposition', 'attachment; filename="cal.ics"');
        return $response;
    }
    /**
     * @Route("/profile/calendar", name="user.calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        return $this->render('booking/calendar.html.twig', [
            'current_menu' => 'vieAsso'
        ]);
    }
    /**
     * @Route("/calendar", name="public.calendar", methods={"GET"})
     */
    public function publicCalendar(): Response
    {
        return $this->render('booking/publicCalendar.html.twig', [
            'current_menu' => 'Events'
        ]);
    }
    /**
     * @Route("/profile/booking/{id}", name="booking_show", methods={"GET"})
     */
    public function show(Booking $booking): Response
    {
        $user = $this->getUser();
        $presence = $booking->getParticipation($user);
        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
            'current_menu' => 'vieAsso',
            'presence' => $presence
        ]);
    }
    /**
     * @Route("/booking/{id}", name="public_booking_show", methods={"GET"})
     */
    public function publicShow(PublicBooking $publicBooking): Response
    {
        return $this->render('booking/publicShow.html.twig', [
            'booking' => $publicBooking,
            'current_menu' => 'Events',
        ]);
    }
    /**
     * @Route("/profile/participate/booking/{id}", name="user.booking.participate")
     */
    public function addParticipant(Booking $booking): Response
    {
        $user = $this->getUser();
        $presence = $booking->getParticipation($user);
        if (!$presence) $booking->addParticipant($user);
        else $booking->removeParticipant($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($booking);
        $entityManager->flush();
        return $this->redirectToRoute('booking_index');
    }
}

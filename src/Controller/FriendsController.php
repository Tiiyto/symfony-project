<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArcherFriendRepository;
use App\Repository\FriendRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FriendsController extends AbstractController
{
    /**
     * @Route("/archers", name="link.archer")
     */
    public function indexArcher(ArcherFriendRepository $archerFriendRepository): Response
    {
        $ArcFriends = $archerFriendRepository->findAll();
        return $this->render('pages/archers.html.twig', [
            'current_menu' => 'archers',
            'ArcFriends' => $ArcFriends
        ]);
    }
    /**
     * @Route("/friends", name="link.friends")
     */
    public function indexFriends(FriendRepository $friendRepository): Response
    {
        $Friends = $friendRepository->findAll();
        return $this->render('pages/archers.html.twig', [
            'current_menu' => 'archers',
            'ArcFriends' => $Friends
        ]);
    }
    /**
     * @Route("/documents", name="link.docs")
     */
    public function indexDocs(): Response
    {
        return $this->render('pages/documents.html.twig', [
            'current_menu' => 'archers',
        ]);
    }
}

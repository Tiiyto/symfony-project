<?php

namespace App\Controller;

use App\Repository\RankingRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RankingController extends AbstractController
{
    /**
     * @Route("/profile/ranking", name="user.classement")
     */
    public function index(RankingRepository $rankingRepository): Response
    {
        return $this->render('pages/classement.html.twig', [
            'current_menu' => 'vieAsso',
            'rank' => $rankingRepository->findOrderByScore()
        ]);
    }
}

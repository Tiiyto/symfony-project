<?php

namespace App\Controller;

use App\Entity\Forum;
use App\Entity\ForumAnswer;
use App\Form\ForumAnswerType;
use App\Form\ForumType;
use App\Repository\ForumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;


/**
 * @Route("/forum")
 */
class ForumController extends AbstractController
{
  /**
   * @Route("", name="forum", methods={"GET"})
   */
  public function index(PaginatorInterface $paginator, ForumRepository $forumRepository, Request $request): Response
  {
    $forums = $paginator->paginate(
      $forumRepository->findAllVisible(),
      $request->query->getInt('page', 1), /*page number*/
      9
    );
    return $this->render('forum/index.html.twig', [
      'current_menu' => 'forum',
      'forums' => $forums,
    ]);
  }

  /**
   * @Route("/new", name="forum_new", methods={"GET","POST"})
   */
  public function new(Request $request): Response
  {
    $forum = new Forum();
    $user = $this->getUser()->getName();
    $form = $this->createForm(ForumType::class, $forum);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $entityManager = $this->getDoctrine()->getManager();
      if ($user) $forum->setAuthor($user);
      else $forum->setAuthor('Anonyme');
      $entityManager->persist($forum);
      $entityManager->flush();
      $this->addFlash('success', 'Votre questiona été posée, elle sera visible lorsque l\'administrateur l\'aura validé');
      return $this->redirectToRoute('forum');
    }

    return $this->render('forum/new.html.twig', [
      'forum' => $forum,
      'current_menu' => 'forum',
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/{slug}-{id}", name="forum_show", requirements={"slug": "[a-z0-9\-]*"})
   */
  public function show(Request $request, Forum $forum): Response
  {
    $forumAnswer = new ForumAnswer();
    $forumAnswer->setAuthor($this->getUser())
      ->setForum($forum);
    $form = $this->createForm(ForumAnswerType::class, $forumAnswer);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($forumAnswer);
      $entityManager->flush();
      return $this->redirectToRoute(
        'forum_show',
        [
          'id' => $forum->getId(),
          'slug' => $forum->getSlug()
        ]
      );
    }
    $form = $this->createForm(ForumAnswerType::class);
    return $this->render('forum/show.html.twig', [
      'forum' => $forum,
      'form' => $form->createView(),
      'current_menu' => 'forum'
    ]);
  }
  /**
   * @Route("/{id}/edit", name="forum_edit", methods={"GET","POST"})
   */
  public function edit(Request $request, Forum $forum): Response
  {
    $form = $this->createForm(ForumType::class, $forum);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->getDoctrine()->getManager()->flush();

      return $this->redirectToRoute('forum');
    }

    return $this->render('forum/edit.html.twig', [
      'forum' => $forum,
      'current_menu' => 'forum',
      'form' => $form->createView(),
    ]);
  }
  /**
   * @Route("/searched", name="forum_searched", methods={"GET"})
   */
  public function find(Request $request, PaginatorInterface $paginator, ForumRepository $forumRepository): Response
  {
    $searched = $request->query->get('search');
    if ($searched == null) return $this->redirectToRoute('forum');
    $forums = $paginator->paginate(
      $forumRepository->findAllSearchedVisible($searched),
      $request->query->getInt('page', 1), /*page number*/
      9
    );
    return $this->render('forum/index.html.twig', [
      'current_menu' => 'forum',
      'forums' => $forums,
      'searched' => $searched
    ]);
  }
  /**
   * @Route("/{id}", name="forum_delete", methods={"DELETE"})
   */
  public function delete(Request $request, Forum $forum): Response
  {
    if ($this->isCsrfTokenValid('delete' . $forum->getId(), $request->request->get('_token'))) {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($forum);
      $entityManager->flush();
    }

    return $this->redirectToRoute('forum');
  }
}
